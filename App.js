import React from "react";
import { Text, View } from "react-native";
import { Router, Stack, Scene, ActionConst } from "react-native-router-flux";

import Login from "./src/Login";
import Wallet from "./src/Wallet";

const App = () => (
  <Router>
    <Stack key="root">
      <Scene
        key="login"
        left={() => null}
        panHandlers={null}
        component={Login}
        title="Login"
      />
      <Scene key="wallet" back={true} component={Wallet} />
    </Stack>
  </Router>
);

export default App;
