# TaurosBeta

El objetivo De la prueba es que uses nuestra API de tauros: [DOCS](https://docs.tauros.io/)`

Como usuario de Tauros beta queremos ver un Login basico que solicite lo siguiente:
    1. Correo electronico
    2. Contraseña
    

El objetivo de tener un login es que puedas obtener el token del usuario para hacer uso de los diferentes endpoints.
    
    
Una vez obtenido el token del usuario queremos ver lo siguiente:

*  Luego del login, queremos que la app nos lleve a una vista donde podamos visualizar mi balance y un listado de wallets como se muestra en la imagen:
   [IMAGEN]( https://drive.google.com/open?id=1MhlirV5cU7TM6jmcBL52lwZMF0u3qD5X)


**RECOMENDACION:** Usar react navigation para el cambio de vista en react-native

**NOTA:** Los estilos no tienen mucha importante, lo mas importante de aca es obtener el balance y el listado de wallets.

