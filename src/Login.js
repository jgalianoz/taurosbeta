import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from "react-native";
import { Actions } from "react-native-router-flux";
import Axios from "axios";
import { LOGIN, TEMP_TOKEN } from "./config/types";

import DeviceInfo from "react-native-device-info";

export default class Login extends Component {
  state = {
    email: "",
    password: "",
    deviceId: "",
    token: ""
  };
  componentDidMount() {
    let deviceId = DeviceInfo.getUniqueId();
    // console.log(deviceId);
    this.setState({ deviceId });
  }
  setLogin = async () => {
    if (this.state.email.trim() === "" || this.state.password.trim() === "") {
      Alert.alert("Alert", "Todos los campos son necesarios");
      return;
    }

    let res = await Axios.post(LOGIN, {
      email: this.state.email,
      password: this.state.password,
      unique_device_id: this.state.deviceId,
      device_name: "Iphone X"
    });

    console.log(res);
    this.setState({ token: res.data.payload.token });
    Actions.wallet({ token: this.state.token, deviceId: this.state.deviceId });
    // if (res.data.success === true && this.state.token === "") {
    //   try {
    //     console.log("aqui es el segundo pedido");
    //     console.log(res.data.payload.token);
    //     let temp = res.data.payload.token;
    //     let dataPost = {
    //       tempToken: temp,
    //       code: "106607",
    //       unique_device_id: this.state.deviceId,
    //       device_name: "Iphone X"
    //     };
    //     let resFinal = await Axios.post(TEMP_TOKEN, dataPost);
    //     console.log(resFinal.data.tempToken);
    //     // this.setState({ token: resFinal.data.tempToken });
    //     // Enviar a otra vista con los saldos
    //   } catch (error) {
    //     console.log("ERROR: ", error);
    //   }
    //   Actions.profile({ token: this.state.token });
    // }
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            onChangeText={email => this.setState({ email })}
          />
        </View>

        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            onChangeText={password => this.setState({ password })}
          />
        </View>

        <TouchableHighlight
          style={[styles.buttonContainer, styles.loginButton]}
          onPress={this.setLogin}
        >
          <Text style={styles.loginText}>Login</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#DCDCDC"
  },
  inputContainer: {
    borderBottomColor: "#F5FCFF",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center"
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: "#FFFFFF",
    flex: 1
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: "center"
  },
  buttonContainer: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 250,
    borderRadius: 30
  },
  loginButton: {
    backgroundColor: "#00b5ec"
  },
  loginText: {
    color: "white"
  }
});
