export const BASE = "https://api.tauros.io/api/v2";
export const LOGIN = "https://api.tauros.io/api/v2/auth/signin/";
export const TEMP_TOKEN = "https://api.tauros.io/api/v2/auth/verify-tfa-email/";
export const REFRESH_TOKEN = "https://api.tauros.io/api/v2/auth/refresh-jwt/";
export const BALANCES = "https://api.tauros.io/api/v1/data/listbalances/";
