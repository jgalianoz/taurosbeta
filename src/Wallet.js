import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Text,
  Icon,
  Right
} from "native-base";
import Axios from "axios";
import { BALANCES, REFRESH_TOKEN, TEMP_TOKEN } from "./config/types";

export default class Wallet extends Component {
  state = {
    finalToken: "",
    balanceData: [
      {
        coin: "LTC",
        balance: 2.0,
        available: 0.0,
        pending: 0.0,
        depositAddress: "DLxcEt3AatMyr2NTatzjsfHNoB9NT62HiF"
      },
      {
        coin: "BTC",
        balance: 14.21549076,
        available: 14.21549076,
        pending: 0.0,
        depositAddress: "1Mrcdr6715hjda34pdXuLqXcju6qgwHA31"
      }
    ]
  };
  componentDidMount() {
    this.refreshToken();
  }
  refreshToken = async () => {
    console.log(this.props.deviceId);
    console.log(this.props.token);
    try {
      response = await Axios.post(TEMP_TOKEN, {
        tempToken: this.props.token,
        code: "106607",
        unique_device_id: this.props.deviceId,
        device_name: "Iphone X"
      });
      console.log(response);
      if (response.success === true) {
        this.setState({ finalToken: response.payload.token });
      }
    } catch (error) {
      console.log("ENTRO AL ERROR: ", error);
      response = await Axios.post(REFRESH_TOKEN, {
        token: this.props.token
      });
      console.log(response);
      if (response.success === true) {
        this.setState({ finalToken: response.payload.token });
        this.getBalances();
      }
    }
    response = await Axios.post(
      "https://api.tauros.io/api/v2/auth/refresh-jwt/",
      { token: this.props.token }
    );
    console.log(response);
    if (response.success === true) {
      this.setState({ finalToken: response.payload.token });
      //   this.getBalances();
    }
  };
  getBalances = async () => {
    try {
      const config = {
        headers: { Authorization: `Token ${this.state.finalToken}` }
      };
      let response = await Axios(BALANCES, config);
      console.log(response);
      if (response.success === true) {
        this.setState({ balanceData: response.data });
      }
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Icon type="Feather" active name="dollar-sign" />
              <Text>Pesos</Text>
              <Right>
                <Text>1.2</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Icon type="FontAwesome" active name="btc" />
              <Text>Bitcoin</Text>
              <Right>
                <Text>{this.state.balanceData[1].balance}</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Icon type="MaterialCommunityIcons" active name="litecoin" />
              <Text>Litecoin</Text>
              <Right>
                <Text>{this.state.balanceData[0].balance}</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Icon type="FontAwesome" active name="btc" />
              <Text>Bitcoin Cash</Text>
              <Right>
                <Text>1.2</Text>
              </Right>
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Icon type="MaterialCommunityIcons" active name="currency-sign" />
              <Text>Stellar</Text>
              <Right>
                <Text>1.2</Text>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
